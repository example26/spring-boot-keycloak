#!/usr/bin/env bash

user=admin1
pass=password
client=client_app
client_secret=1e1a10f1-cef8-4f81-a0ad-6d4ed371c285

echo "Requesting token"

token=`curl -X POST -H "content-type:application/x-www-form-urlencoded" -d "grant_type=password&client_id=$client&client_secret=$client_secret&username=$user&password=$pass" http://vdebian:8181/auth/realms/demo/protocol/openid-connect/token | jq .access_token | sed 's/"//g'`

echo ""
echo "Token received"
echo "----------------------------------------"
echo ""

echo $token

echo ""
echo "----------------------------------------"

echo "Requesting regular service"
curl -H "authorization: Bearer $token" http://localhost:8080/secured/regular

echo ""

echo "Requesting admin service"
curl -H "authorization: Bearer $token" http://localhost:8080/secured/admin

echo ""
