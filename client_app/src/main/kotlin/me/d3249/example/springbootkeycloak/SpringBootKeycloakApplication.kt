package me.d3249.example.springbootkeycloak

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringBootKeycloakApplication

fun main(args: Array<String>) {
	runApplication<SpringBootKeycloakApplication>(*args)
}
