package me.d3249.example.springbootkeycloak

import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("/secured")
class SecuredController {

    @RolesAllowed("regular")
    @GetMapping("/regular")
    fun regular(): Mono<StringWrapper> = Mono.just(StringWrapper("Welcome regular user!"));

    @RolesAllowed("admin")
    @GetMapping("/admin")
    fun admin(): Mono<StringWrapper> = Mono.just(StringWrapper("Welcome admin user!"));
}

data class StringWrapper(val message: String)