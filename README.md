# Keycloak POC

This is a hands-on learning project. The final goal is to be able to secure spring applications with keycloak on a microservices architecture.

The project will be containerized, using docker-compose as basic tool. All configurations should be portable to kubernetes.

The project is split on authentication service and a client app.
