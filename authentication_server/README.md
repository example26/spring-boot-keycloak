# Authentication Service

## Configurations

In order to deploy the following variables *MUST* be declared within a `.env` file

```bash
KC_PORT=#[Port to expose keycloak web console]
KC_ADM=#[Admini user]
KC_PWD=#[Password for admin user]

DB_USR=#[Database user]
DB_PWD=#[Database password]
DB_NAME=#[Database name]
DB_DATA_PATH=#[Path to mount database data on the host]

```

## Basic Setup

1. Create a realm
1. Create a client (A client is an app/service wich can request tokens)
    - Client name
    - Client Protocol (Use openid-connect)
    - Root URL

    - Configurations/Acces Type: confidential
    - Configurations/Authorization Enabled: On
    - Configurations/Service Account Enabled: On

    - Credentials/Client Secret (Copy for future use)

    - For multi client authentication, create client roles
1. Create realm roles (User roles)
1. If needed, create initial user

## Request token

Go to Realm Settings -> General and click on Open Id Endpoints Configuration.

`Token endpoin` is the endpoint to generate a token

Token MUST be requested by a `POST` request with header `Content-Type: application/x-www-form-urlencoded`.

The following are required values

|Field | Description |
|------|-------------|
| grant\_type | password |
| client\_id | [as created] |
| client\_secret | [as generated] |
| username | [as required] |
| password | [as requierd] |

This is a cURL example

```bash
curl -X POST '<KEYCLOAK_SERVER_URL>/auth/realms/<REALM_NAME>/protocol/openid-connect/token' \
 --header 'Content-Type: application/x-www-form-urlencoded' \
 --data-urlencode 'grant_type=password' \
 --data-urlencode 'client_id=<CLIENT_ID>' \
 --data-urlencode 'client_secret=<CLIENT_SECRET>' \
 --data-urlencode 'username=<USERNAME>' \
 --data-urlencode 'password=<PASSWORD>'

```

